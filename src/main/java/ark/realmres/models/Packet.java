package ark.realmres.models;

import org.jdom2.Element;
import org.json.JSONObject;

public class Packet {
    String name;
    String id;

    public Packet(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public Element toXMLElement() {
        Element e = new Element("Packet");
        e.addContent(new Element("PacketName").setText(this.name.replace("_", "")));
        e.addContent(new Element("PacketId").setText(this.id));
        return e;
    }
}
