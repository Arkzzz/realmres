package ark.realmres.util;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class FileUtils {
    public static String readFile(String file) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine();
            while (line != null) {
                sb.append(line).append(System.lineSeparator());
                line = br.readLine();
            }
        }
        return sb.toString();
    }

    public static String readFileNoBreak(String file) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
        }
        return sb.toString();
    }

    public static void downloadFile(String uri, String to) throws IOException {
        try (ReadableByteChannel rbc = Channels.newChannel(Http.getStream(uri))) {
            FileOutputStream fos = new FileOutputStream(to);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
        }
    }

    public static void writeFile(String content, String file) throws IOException {
        FileWriter fw = new FileWriter(file, false);
        fw.write(content);
        fw.close();
    }
}
