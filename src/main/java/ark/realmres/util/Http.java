package ark.realmres.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Http {
    public static String get(String uri, int timeout) throws IOException {
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        return get(conn, timeout);
    }


    private static String get(HttpURLConnection conn, int timeout) throws IOException {
        StringBuilder result = new StringBuilder();
        conn.setRequestMethod("GET");
        conn.setConnectTimeout(timeout);
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        conn.disconnect();
        return result.toString();
    }

    public static String get(String uri) throws IOException {
        return get(uri, 5000);
    }

    public static InputStream getStream(String uri) throws IOException {
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setConnectTimeout(5000);
        return conn.getInputStream();
    }
}

