package ark.realmres.util;

import ark.realmres.models.Packet;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Updater {
    public static final String PUBLIC_PATH = Paths.get(System.getProperty("user.dir"), "public").toString();

    public static final String WORKSPACE_PATH = Paths.get(System.getProperty("user.dir"), "working").toString();
    public static final String FFDEC_PATH = Paths.get(System.getProperty("user.dir"), "ffdec", "ffdec.jar").toString();
    public static final String WORKSPACE_CLIENT_PATH = Paths.get(WORKSPACE_PATH, "client.swf").toString();
    public static final String GSC_PATH = Paths.get(WORKSPACE_PATH, "scripts", "kabam", "rotmg", "messaging", "impl", "GameServerConnection.as").toString();

    public static final String VERSION_PATH = Paths.get(PUBLIC_PATH, "version.txt").toString();

    public static final String OBJECTS_XML_PATH = Paths.get(PUBLIC_PATH, "objects.xml").toString();
    public static final String TILES_XML_PATH = Paths.get(PUBLIC_PATH, "tiles.xml").toString();
    public static final String PACKETS_XML_PATH = Paths.get(PUBLIC_PATH, "packets.xml").toString();

    public static final String OBJECTS_JSON_PATH = Paths.get(PUBLIC_PATH, "objects.json").toString();
    public static final String TILES_JSON_PATH = Paths.get(PUBLIC_PATH, "tiles.json").toString();
    public static final String PACKETS_JSON_PATH = Paths.get(PUBLIC_PATH, "packets.json").toString();

    public static final String STATIC_DRIPS = "https://static.drips.pw/rotmg/production/current/";
    public static final String REMOTE_VERSION = STATIC_DRIPS + "version.txt";
    public static final String REMOTE_CLIENT = STATIC_DRIPS + "client.swf";

    public static final String REMOTE_OBJECTS_XML = STATIC_DRIPS + "xmlc/Objects.xml";
    public static final String REMOTE_TILES_XML = STATIC_DRIPS + "xmlc/GroundTypes.xml";

    public static final String REMOTE_OBJECTS_JSON = STATIC_DRIPS + "json/Objects.json";
    public static final String REMOTE_TILES_JSON = STATIC_DRIPS + "json/GroundTypes.json";

    private static final SimpleDateFormat DATE_FMT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    private static final Pattern PACKET_PATTERN = Pattern.compile("static const ([A-Z_]+):int = (\\d+);");

    static boolean UPDATE_IN_PROGRESS = false;

    public static boolean updatesAvailable() {
        while (UPDATE_IN_PROGRESS) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            String remoteVersion = getRemoteVersion();
            String localVersion = getLocalVersion();
            return !remoteVersion.equals(localVersion);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void update() {
        if (!UPDATE_IN_PROGRESS) {
            UPDATE_IN_PROGRESS = true;
            long startTime = System.currentTimeMillis();
            log("Starting update!");
            try {
                downloadStaticDrips();
                updatePackets();
            } catch (IOException e) {
                log("Error while updating!");
                e.printStackTrace();
            }
            log("Update finished in " + (System.currentTimeMillis() - startTime) + "ms");
            UPDATE_IN_PROGRESS = false;
        }
    }

    public static void updatePackets() throws IOException {
        FileUtils.downloadFile(REMOTE_CLIENT, WORKSPACE_CLIENT_PATH);
        log("Saving client.swf");
        try {
            if (Runtime.getRuntime().exec(String.format("java -jar %s -selectclass kabam.rotmg.messaging.impl.GameServerConnection -export script %s %s",
                    FFDEC_PATH,
                    WORKSPACE_PATH,
                    WORKSPACE_CLIENT_PATH)).waitFor() != 0) throw new IOException("Unable to unpack GameServerConnection.as");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log("Extracted GameServerConnection.as");
        ArrayList<Packet> packets = new ArrayList<>();
        Matcher pm = PACKET_PATTERN.matcher(FileUtils.readFile(GSC_PATH));
        while (pm.find()) {
            packets.add(new Packet(pm.group(1), pm.group(2)));
        }
        log("Loaded packets!");
        Element xml = new Element("Packets");
        JSONObject json = new JSONObject();
        for (Packet p : packets) {
            xml.addContent(p.toXMLElement());
            json.put(p.getName(), Integer.parseInt(p.getId()));
        }
        XMLOutputter xmlout = new XMLOutputter();
        xmlout.setFormat(Format.getCompactFormat());
        FileWriter xmlwriter = new FileWriter(PACKETS_XML_PATH, false);
        xmlout.output(new Document(xml), xmlwriter);
        xmlwriter.close();
        log("Saving packets.xml");
        FileUtils.writeFile(json.toString(), PACKETS_JSON_PATH);
        log("Saving packets.json");

    }

    static void downloadStaticDrips() throws IOException {
        FileUtils.downloadFile(REMOTE_OBJECTS_XML, OBJECTS_XML_PATH);
        log("Saving objects.xml");
        FileUtils.downloadFile(REMOTE_TILES_XML, TILES_XML_PATH);
        log("Saving tiles.xml");
        FileUtils.downloadFile(REMOTE_OBJECTS_JSON, OBJECTS_JSON_PATH);
        log("Saving objects.json");
        FileUtils.downloadFile(REMOTE_TILES_JSON, TILES_JSON_PATH);
        log("Saving tiles.json");
        FileUtils.downloadFile(REMOTE_VERSION, VERSION_PATH);
        log("Saving version.txt");
    }

    static String getLocalVersion() throws IOException {
        return FileUtils.readFileNoBreak(VERSION_PATH);
    }

    static String getRemoteVersion() throws IOException {
        return Http.get(REMOTE_VERSION);
    }

    static void log(String s) {
        System.out.println(DATE_FMT.format(new Date()) + " UPDATER " + s);
    }
}
