package ark.realmres;

import ark.realmres.util.Updater;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.nio.file.Paths;

@RestController
public class RealmResApi {
    @RequestMapping(value="/{fileKey}", method = RequestMethod.GET)
    @ResponseBody
    public FileSystemResource getUserFile(HttpServletResponse response, @PathVariable String fileKey){
        response.setContentType("application/*");
        if (Updater.updatesAvailable()) Updater.update();
        final File file = new File(Paths.get(Updater.PUBLIC_PATH, fileKey).toString());
        return new FileSystemResource(file);
    }
}
