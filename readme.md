# Realm Res
This is a restful api for retrieving RotMG assets, a public api server can be found at https://mysterious-inlet-24356.herokuapp.com/
# Api Docs
## Paths
It is recomended to set the request timeout high as the updater may take some time.
### /
 - version.txt The RoTMG build version that the assets are updated to.
 - objects.xml - Objects
 - tiles.xml - Tiles
 - packets.xml - Packets
 - objects.json - Objects
 - tiles.json - Tiles
 - packets.json - Packets
